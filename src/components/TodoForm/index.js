import React, {useState, useContext} from 'react';
import { v4 as uuidv4 } from 'uuid';
import TodoContext from "../todo-context/TodoContext";
import classes from './style.module.scss';
import TodoList from "../TodoList";

const TodoForm = () => {
  const ctx = useContext(TodoContext);
  const [value, setValue] = useState('');
  const handleChange = (e) => {
    setValue(e.target.value);
  }

  const addItem = () => {
    const item = {
      id: uuidv4(),
      value
    }
    setValue('')
    ctx.addItem(item);
  }

  return (
    <div className={classes.todoForm}>
      <div className={classes.text}>Todo list</div>
      <div className={classes.formInput}>
        <input value={value} onChange={handleChange} type={'text'} placeholder={'Add a todo'}/>
        <div className={classes.add} onClick={addItem}>Add</div>
      </div>
      <TodoList/>
    </div>
  )
}

export default TodoForm
