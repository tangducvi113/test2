import React, {useContext, useState} from 'react';
import classes from "./style.module.scss";

const TodoList = ({ item, handleDelete, handleEdit, isToggleEdit, toggleEdit, itemEdit }) => {
  const handleValueEdit = (e) => {

  }

  const handleToggleEdit = () => {
    toggleEdit();
    handleEdit(item);
  }

  const handleUpdateItem = () => {
    // const itemEdit = {
    //   id: item.id,
    //   value: valueEdit
    // }
    toggleEdit();
    handleEdit(item);
  }

  return (
    <div className={classes.todoItem}>
      <div className={classes.formInput}>
        <input onChange={handleValueEdit} value={item.value} type={'text'}/>
        {/*<div className={classes.add} onClick={updateItem}>Update</div>*/}
      </div>
      {
        !isToggleEdit && (
          <div className={classes.actions}>
            <img onClick={handleToggleEdit} className={classes.edit} src={'edit-icon.svg'} alt={''}/>
            <img onClick={() => handleDelete(item.id)} className={classes.delete} src={'delete-icon.svg'} alt={''}/>
          </div>
        )
      }

      {
        isToggleEdit && (
          <div className={classes.actions}>
            <img className={classes.edit} src={'white-theme-icon.svg'} alt={''}/>
            <div onClick={handleUpdateItem} className={classes.update}>Update</div>
          </div>
        )
      }
    </div>
  )
}

export default TodoList
