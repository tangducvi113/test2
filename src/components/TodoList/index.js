import React, {useContext, useState} from 'react';
import TodoContext from "../todo-context/TodoContext";
import classes from "./style.module.scss";
import TodoItem from "./TodoItem";

const TodoList = () => {
  const [itemEdit, setItemEdit] = useState(null);
  const ctx = useContext(TodoContext);
  const { items, isToggleEdit } = ctx;
  const handleDelete = (id) => {
    ctx.deleteItem(id);
  }

  const handleEdit = (item) => {
    setItemEdit(item);
    ctx.editItem(item)
  }

  const toggleEdit = () => {
    ctx.toggleEdit();
  }

  return (
    <div className={classes.todoList}>
      {
        items.map((item, idx) => (
          <TodoItem key={idx} item={item} handleDelete={handleDelete} handleEdit={handleEdit} itemEdit={itemEdit} isToggleEdit={isToggleEdit} toggleEdit={toggleEdit}/>
        ))
      }
    </div>
  )
}

export default TodoList
