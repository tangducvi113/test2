import classes from './Header.module.scss';

const Header = () => {
  return (
    <header className={classes.header}>
      <div className={classes.title}>My work</div>
      <img src={'/avatar.png'} alt={''}/>
    </header>
  );
};

export default Header;
