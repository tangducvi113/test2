import React from 'react';

const TodoContext = React.createContext({
  items: null
})

export default TodoContext
