import TodoContext from "./TodoContext";
import {ADD_ITEM, DELETE_ITEM, EDIT_ITEM, TOGGLE_EDIT} from "../constant";
import {useReducer} from "react";

const initialState = {
  items: [],
  isToggleEdit: false
}

const todoReducer = (state = initialState, action) => {
  switch(action.type) {
    case ADD_ITEM:
      return {
        ...state,
        items: [...state.items, action.payload]
      }
    case DELETE_ITEM:
      console.log(action)
      const cloneItems = [...state.items]
      return {
        ...state,
        items: cloneItems.filter(item => item.id !== action.payload.id)
      }
    case EDIT_ITEM:
      return;
    case TOGGLE_EDIT:
      return {
        ...state,
        isToggleEdit: !state.isToggleEdit
      }
    default:
      return state;
  }
}

const TodoProvider = (props) => {
  const [todoState, dispatch] = useReducer(todoReducer, initialState);

  const deleteItem = (id) => {
    dispatch({ type: DELETE_ITEM, payload: {id}})
  }

  const addItem = (item) => {
    dispatch({ type: ADD_ITEM, payload: item})
  }

  const editItem = (item) => {
    console.log(item)
    // dispatch({ type: EDIT_ITEM, payload: item})
  }

  const toggleEdit = () => {
    dispatch({ type: TOGGLE_EDIT })
  }

  const ctx = {
    items: todoState.items,
    isToggleEdit: todoState.isToggleEdit,
    deleteItem,
    addItem,
    editItem,
    toggleEdit
  }
  return (
    <TodoContext.Provider value={ctx}>
      {props.children}
    </TodoContext.Provider>
  )
}

export default TodoProvider;
